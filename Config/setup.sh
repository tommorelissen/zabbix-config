#!/bin/bash

sudo wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+$(lsb_release -sc)_all.deb 
sudo dpkg -i zabbix-release_5.0-1+$(lsb_release -sc)_all.deb


sudo chmod 775 /var/log/mail.log
sudo chmod 775 /var/log/apache2/access.log
sudo chmod 775 /var/log/apache2/error.log
sudo chmod 775 /var/log/auth.log
sudo chmod 775 /var/log/mysql/error.log
sudo chmod 775 /var/log/apache2/other_vhosts_access.log
sudo chmod 775 /var/log/syslog
sudo chmod 775 /var/log/dpkg.log

sudo systemctl restart zabbix-agent 
sudo systemctl enable zabbix-agent

sudo ufw allow 10050/tcp
