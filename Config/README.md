## Zabbix agent setup

1. Add a clear hostname to the server

2. Download the zabbix agent:
```shell
sudo wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+$(lsb_release -sc)_all.deb 

sudo dpkg -i zabbix-release_5.0-1+$(lsb_release -sc)_all.deb

sudo apt update

sudo apt -y install zabbix-agent
```

3. Paste the config files out of zabbix-conf to /etc/zabbix/

4. Setup log files with correct access rights

```shell

sudo chmod -R 775 /var/log/mysql
sudo chmod -R 775 /var/log/apache2
sudo chmod 775 /var/log/mail.log
sudo chmod 775 /var/log/auth.log
sudo chmod 775 /var/log/syslog
sudo chmod 775 /var/log/dpkg.log

```
5. Setup mysql user
```shell
sudo mysql -u root -p
```
```sql
CREATE USER 'zabbix'@'%';

GRANT USAGE,REPLICATION CLIENT,PROCESS,SHOW DATABASES,SHOW VIEW ON *.* TO 'zabbix'@'%';
```

6. Setup zabbix log file
```shell 
sudo mkdir /var/log/zabbix-agent
sudo touch /var/log/zabbix-agent/zabbix_agentd.log
sudo chmod 777 /var/log/zabbix-agent/zabbix_agentd.log
```

6. Start Zabbix agent and set it to boot on startup

```shell
sudo systemctl restart zabbix-agent 

sudo systemctl enable zabbix-agent
```

7. Step 4: Configure firewall for the Zabbix agent

```shell
sudo ufw allow 10050/tcp
```

8. Setup crontab jobs
```cron
* * * * *  chmod -R 775 /var/log/mysql
* * * * *  chmod -R 775 /var/log/apache2
* * * * *  chmod 775 /var/log/mail.log
* * * * *  chmod 775 /var/log/auth.log
* * * * *  chmod 775 /var/log/syslog
* * * * *  chmod 775 /var/log/dpkg.log
```

9. Setup Apache status page, copy apache2-conf/status.conf file to /etc/apache2/mods-enabled/status.conf


